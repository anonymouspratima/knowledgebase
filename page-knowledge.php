<?php get_header(); ?>
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <div class="jumbotron-details">
            <h1>Knowledge Base</h1>      
        </div>
    </div>
</div> 
<div class="section-2">
<h1>Featured Articles</h1>
<div class="container">
            
        <div id="article">
        
<?php

    $args = array(
    	'post_type' => 'knowledge_base',
    );
    $your_loop = new WP_Query( $args );

    if ( $your_loop->have_posts() ) : while ( $your_loop->have_posts() ) : $your_loop->the_post();
    $meta = get_post_meta( $post->ID, 'your_fields', true ); ?>
    <!-- contents of Your Post -->
 
            
                
                    <div class="column-1">
                        <div class="thumbnail">
                            <?php 
                          if ( has_post_thumbnail() ) {
                          the_post_thumbnail();
                          }  ?>
                        </div>
                        <div class="details">
                        <h3 class="article-title"><?php the_title(); ?></h3>
                        <p><?php the_content(); ?></p>
                        </div>
                    </div>
                
            
            
     

    <?php endwhile; endif; wp_reset_postdata(); ?>
    </div>
    </div>
    </div>
    <?php get_footer(); ?>