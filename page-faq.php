<?php get_header(); ?>
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <div class="jumbotron-details">
            <h1>FAQ Section</h1>      
        </div>
    </div>
</div> 
<?php

    $args = array(
    	'post_type' => 'faq',
    );
    $your_loop = new WP_Query( $args );

    if ( $your_loop->have_posts() ) : while ( $your_loop->have_posts() ) : $your_loop->the_post();
    $meta = get_post_meta( $post->ID, 'your_fields', true ); ?>
   

<div class="main-faq">
    <div class="container">
        <h2>Simple Collapsible</h2>
        <a href="#demo" class="btn btn-primary" data-toggle="collapse"><?php the_title(); ?></a>     
    <div id="demo" class="collapse">
        <?php the_content(); ?>
    </div>
    </div>
</div>
    <?php endwhile; endif; wp_reset_postdata(); ?>

    <?php get_footer(); ?>