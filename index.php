<?php get_header(); ?>
<main>
  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <div class="jumbotron-details">
        <h1>CMST Knowledge Base</h1>      
        <p>Makes you easier to learn</p>
        <!-- Search form -->
        
        <?php get_search_form(); ?>
      </div>
    </div>
  </div>
  <div class="section-1">
    <div class="row">
      <div class="container">
          <div class="column-1">
              <div class="col-icon">
                      <i class='fas fa-graduation-cap' style='font-size:6rem'></i>
              </div>
              <div class="col-title">Knowledge Base</div>
              <div class="col-description">Our knowledge base is growing rapidly with new tutorials & how-to’s. Make sure to search the knowledge base before submitting a ticket.​</div>
              <a href="#" class="btn green" role="button">View more</a>
          </div>
          <div class="column-2">
              <div class="col-icon">
                      <i class='fas fa-chalkboard-teacher' style='font-size:6rem'></i>
              </div>
              <div class="col-title">FAQ</div>
              <div class="col-description">Our knowledge base is growing rapidly with new tutorials & how-to’s. Make sure to search the knowledge base before submitting a ticket.​</div>
              <a href="#" class="btn green" role="button">View more</a>
          </div>
          <div class="column-3">
              <div class="col-icon">
                      <i class='fas fa-book-open' style='font-size:6rem'></i>
              </div>
              <div class="col-title">Support Forum</div>
              <div class="col-description">Our knowledge base is growing rapidly with new tutorials & how-to’s. Make sure to search the knowledge base before submitting a ticket.​</div>
              <a href="#" class="btn green" role="button">View more</a>
          </div>
      </div>
    </div>
  </div>
<div class="section-2">
<h1>Featured Articles</h1>
<div class="container">
            
        <div id="article">
        
<?php

    $args = array(
    	'post_type' => 'knowledge_base',
    );
    $your_loop = new WP_Query( $args );

    if ( $your_loop->have_posts() ) : while ( $your_loop->have_posts() ) : $your_loop->the_post();
    $meta = get_post_meta( $post->ID, 'your_fields', true ); ?>
    <!-- contents of Your Post -->
                    <div class="column-1">
                        <div class="thumbnail">
                            <?php 
                          if ( has_post_thumbnail() ) {
                          the_post_thumbnail();
                          }  ?>
                        </div>
                        <div class="details">
                        <h3 class="article-title"><?php the_title(); ?></h3>
                        <p><?php the_content(); ?></p>
                        </div>
                    </div>
    <?php endwhile; endif; wp_reset_postdata(); ?>
    </div>
    <?php get_sidebar(); ?>
    </div>
    </div>
    
</main>

<?php get_footer(); ?>