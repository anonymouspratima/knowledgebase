<?php
function Site_enqueue() {
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
    $dependencies = array('bootstrap');
    wp_enqueue_style( 'main-style', get_stylesheet_uri(), $dependencies ); 
    wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css' );

    $dependencies = array('jquery');
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', $dependencies, '4.3.1', true );
}
add_action('wp_enqueue_scripts', 'Site_enqueue');

function site_wp_setup() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    
}
 
add_action( 'after_setup_theme', 'site_wp_setup' );

register_nav_menus(array(
    'primary' => __('Primary Menu')
));

function Site_widgets_init() {
  register_sidebar( array(
    'name'          => 'Footer 1',
    'id'            => 'footer_1',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="ttl">',
    'after_title'   => '</h4>',
  ) );
  register_sidebar( array(
    'name'          => 'Footer 2',
    'id'            => 'footer_2',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="ttl">',
    'after_title'   => '</h4>',
  ) );
  register_sidebar( array(
    'name'          => 'Footer 3',
    'id'            => 'footer_3',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="ttl">',
    'after_title'   => '</h4>',
  ) );
  register_sidebar( array(
    'name'          => 'sidebar',
    'id'            => 'sidebar',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="ttl">',
    'after_title'   => '</h4>',
  ) );
  register_sidebar( array(
    'name'          => 'Footer - Copyright Text',
    'id'            => 'footer_copyright_text',
    'before_widget' => '<div class="footer_copyright_text">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>',
) );
}
add_action( 'widgets_init', 'Site_widgets_init' );

    function create_post_your_post() {
    	register_post_type( 'faq',
    		array(
    			'labels'       => array(
                'name' => __( 'FAQ' ),
    			),
    			'public'       => true,
    			'hierarchical' => true,
    			'has_archive'  => true,
    			'supports'     => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
    			),
    			'taxonomies'   => array(
                'post_tag',
                'category',
    			)
    		)
        );
        register_post_type( 'knowledge_base',
    		array(
    			'labels'       => array(
                'name' => __( 'Knowledge Base' ),
    			),
    			'public'       => true,
    			'hierarchical' => true,
    			'has_archive'  => true,
    			'supports'     => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
    			),
    			'taxonomies'   => array(
                'post_tag',
                'category',
    			)
    		)
    	);
    	register_taxonomy_for_object_type( 'category', 'knowledge_base' );
    	register_taxonomy_for_object_type( 'post_tag', 'knowledge_base' );
    }
    add_action( 'init', 'create_post_your_post' );


?>