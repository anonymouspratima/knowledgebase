<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="mobile-view.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
    <div class="top-header">
        <!-- Brand/logo -->
        <div class="container">
        <div class="branding">
                <a class="brand" href="#">
                    <img src="bird.jpg" alt="logo">
                </a>
        </div>
        <div class="info-area">
            <div class="icon">
                <i class='fas fa-phone' style='font-size:36px'></i>
            </div>
            <div class="info-text">
            <div class="info-title">Call Us Now</div>
            <div class="info-detail">9876283679</div>
            </div>
        </div>
        </div>
    </div>
        <nav class="navbar navbar-expand-sm">
            <div class="container">
                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                        <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Links -->
                
				<?php 
				$args = array(
					'theme_location' => 'primary'
				);
				?>
				<?php wp_nav_menu($args); ?>
            </div>
        </nav>
              
</header>